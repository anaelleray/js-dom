let paraList = document.querySelectorAll('p');//[p, p, p, p, p]
let inputColor = document.querySelector('#highlight-color');


//On itère sur les paragraphes capturés précédemment
for (const para of paraList) {
    //A chaque tour, para contient un des paragraphe du tableau
    //sur lequel on ajoute un eventListener au click
    para.addEventListener('click', function () {
        para.style.backgroundColor = inputColor.value;
    });
    para.addEventListener('dblclick', function () {
        para.style.backgroundColor = 'unset';
    });

}

/* Bonus
inputColor.addEventListener('change', function () {
    let color = inputColor.value;
    for (const para of paraList) {
        if (para.style.backgroundColor && para.style.backgroundColor !== 'unset') {
            para.style.backgroundColor = color;
        }
    }
});
*/

