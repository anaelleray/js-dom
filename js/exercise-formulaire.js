// Etape 1 : sélectionner cette section pour pouvoir ajouter nos futurs form-group
let questionsSection = document.querySelector("#questions");
// console.log(questionsSection);

// Etape 2 : créer et ajouter div avec une classe form-group
// let newDiv = document.createElement("div");
// newDiv.classList.add("form-group");
// console.log(newDiv);
// newDiv.textContent = "Je suis ici"; //ou mettre une border avec width height
// questionsSection.appendChild(newDiv);

// Etape 3 : Conditionner avec l'événement : click du button
let button = document.querySelector("#addQuestion");
button.addEventListener("click", function(){
    // let newDiv = document.createElement("div");
    // newDiv.classList.add("form-group");
    // console.log(newDiv);
    // newDiv.textContent = "Je suis ici"; //ou mettre une border avec width height
    // questionsSection.appendChild(newDiv);

    // Etape 4 : creer et ajouter label question 
        // + div form-check form-check-inline avec label et input x2

    let leveljsSelector = document.querySelector("#leveljs");
    let newFormGroup = document.querySelector("#formgroup2");
    let questionLabel = document.createElement("label");

    if (leveljsSelector.value <= 3){
        questionLabel.textContent = "Do you know query selector ?";
    } else{
        questionLabel.textContent = "Do you know JS object ?";
    }
    
    //Création div qui contient le premier label et le premier input
    let formCheck1 = document.createElement("div");
    formCheck1.classList.add("form-check","form-check-inline");

    let inputCheck1 = document.createElement("input");
    inputCheck1.classList.add("form-check-input");
    inputCheck1.setAttribute("type","radio");
    inputCheck1.setAttribute("id","a1");
    inputCheck1.setAttribute("name","a");
    inputCheck1.setAttribute("value","option1");

    let labelCheck1 = document.createElement("label");
    labelCheck1.classList.add("form-check-label");
    labelCheck1.setAttribute("for", "a1");
    labelCheck1.textContent = "YES";

    //Création div qui contient le premier label et le premier input
    let formCheck2 = document.createElement("div");
    formCheck2.classList.add("form-check","form-check-inline");

    let inputCheck2 = document.createElement("input");
    inputCheck2.classList.add("form-check-input");
    inputCheck2.setAttribute("type","radio");
    inputCheck2.setAttribute("id","a2");
    inputCheck2.setAttribute("name","a");
    inputCheck2.setAttribute("value","option2");

    let labelCheck2 = document.createElement("label");
    labelCheck2.classList.add("form-check-label");
    labelCheck2.setAttribute("for", "a2");
    labelCheck2.textContent = "NO";

    //on lie nos éléments
    newFormGroup.appendChild(questionLabel);

    formCheck1.appendChild(inputCheck1);
    formCheck1.appendChild(labelCheck1);
    newFormGroup.appendChild(formCheck1);

    formCheck2.appendChild(inputCheck2);
    formCheck2.appendChild(labelCheck2);
    newFormGroup.appendChild(formCheck2);
    
});
