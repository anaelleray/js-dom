//document.querySelector("#para2").style.color = "blue"
//also possible
//mais les variables permettent de faire plusieurs actions sur l'élement sans le re-déclarer

let para2 = document.querySelector("#para2");
para2.style.color = "pink";

let border = document.querySelector("#section2");
border.style.border = "dotted magenta";

let bg = document.querySelector("#section2 .colorful")
bg.style.backgroundColor = "purple"

let titre = document.querySelector("#section1 h2");
titre.style.fontStyle = "italic";

let hide = document.querySelector("p .colorful");
hide.style.display = "none";
//hide.style.visibility = "hidden";

para2.textContent="modified by JS";

let lien = document.querySelector("#section1 a");
lien.href = "https://simplonlyon.fr";

let h2Sect2 = document.querySelector("#section2 h2");
h2Sect2.classList.add("big-text");
//h2Sect2.className("big-text");

let italic =document.querySelectorAll("p");
//item pour itérer les éléments de la variable, l'item va se "gérer tout seul" pour voir sur quels éléments il doit se fixer
// For Of va itérer tout seul et se terminer tout seul, il continu tant qu'il peut sinon il s'arrête.
for (const item of italic) {
    //et donc pour chaque item on applique un style
    item.style.fontStyle = "italic";
}
//selecteur All va créer un TABLEAU donc créer une boucle pour lire le tableau


