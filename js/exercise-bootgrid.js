let colList = document.querySelectorAll('.exo-col');
let selectJustify = document.querySelector('#select-justify');
let selectAlign = document.querySelector('#select-align');
let row = document.querySelector('.row');
let colInput = document.querySelector('#col-input');
let showInput = document.querySelector('#show-input');


for (const col of colList) {

    col.classList.add('col-4');
}

let currentJustify = 'justify-content-between';
selectJustify.addEventListener('change', function () {

    row.classList.replace(currentJustify, selectJustify.value);

    currentJustify = selectJustify.value;



});
let currentAlign = 'align-items-start';
selectAlign.addEventListener('change', function () {

    row.classList.replace(currentAlign, selectAlign.value);

    currentAlign = selectAlign.value;

});

let currentCol = 'col-4';
colInput.addEventListener('change', function () {

    for (const col of colList) {
        col.classList.replace(currentCol, 'col-' + colInput.value);
    }
    currentCol = 'col-' + colInput.value;

});

showInput.addEventListener('change', function () {

    for (let index = 0; index < colList.length; index++) {
        const col = colList[index];

        if (index < showInput.value) {
            col.style.display = 'block';
        } else {
            col.style.display = 'none';
        }

    }

});
