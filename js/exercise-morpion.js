//Au clic faire apparaitre une croix ou un rond

//Solution 1 :faire apparaitre des img (emojis ou autre)
//Solution 2 :de mettre un textContent (emojis) avec X ou O + classe css
//Solution 3 : faire des shapes CSS rond et croix et appliquer les classes

// Etape 1 : ajout croix à la section 
// document.addEventListener("click", function(){
//     let mainSection = document.querySelector("#morpion");

//     //Ajouter classe rond ou croix aux enfants ?
//     let newCross = document.createElement("div");
//     newCross.classList.add("cross");

//     mainSection.appendChild(newCross);
// });

//Etape 2 : cliquer case1 et faire apparaitre la croix
// let case1 = document.querySelector(".case1");

// case1.addEventListener("click", function(){
//     //Ajouter classe rond ou croix aux enfants ?
//     let newCross = document.createElement("div");
//     newCross.classList.add("cross");

//     case1.appendChild(newCross);
// });

//Etape 3 : faire clic + apparaitre croix sur toutes les cases
let cases = document.querySelectorAll(".col-4");  //renvoie un array d'élement HTML séléctionné par un sélécteur css

//Etape 4 : Mettre un compteur pour en fx des tours changer la forme
let compteurTourDeJoueur = 0;

for (let index = 0; index < cases.length; index++) {
    //0.addEventListener : FAUX
    //cases[0].addEventListener
    //case1HTML.addEventListener

    cases[index].addEventListener("click", function () {

        //Etape 6 : si toutes les cases pleines on recharge la grille
        if (compteurTourDeJoueur === 9) {
            alert("Rechargeons la page ?");
            document.location.reload();
        }

        //Etape 5 : on ne peut pas ajouter plusieurs formes dans la même case        
        // console.log(cases[index].hasChildNodes());
        // console.log("début " + compteurTourDeJoueur);

        if (!cases[index].hasChildNodes()) {
            let newShape = document.createElement("div");
            if (compteurTourDeJoueur % 2 === 0) {
                // Ajouter classe rond ou croix aux enfants
                newShape.classList.add("cross");
                cases[index].classList.add("played");
                cases[index].appendChild(newShape);
                compteurTourDeJoueur++;
            } else {
                newShape.classList.add("round");
                cases[index].appendChild(newShape);
                cases[index].classList.add("played");
                compteurTourDeJoueur++;
            }

        } else if (cases[index].hasChildNodes() && compteurTourDeJoueur < 9) {
            alert("Vous avez déjà joué sur cette case !! Retente ta chance !");
        }

        //Etape 7 : qui a gagné ? 
        // comment vérifier si trois div sont alignées sur la grille
        // comment vérifier la présence des mêmes classes CSS 
        // dans 3 boxes l'une à coté de l'autre sur une ligne
        // ou 3 boxes l'une au dessus de l'autre sur une colonne
        // ou 3 boxes en diagonale
        // PS : trouver les index dans notre tableau de caseHTML


        // Solution Gain 1 : 
        isWinner(cases);

        // console.log("fin du clic " + compteurTourDeJoueur);
    });

}

function isWinner(tab) {
    if (tab[0].classList.contains("played")
        && tab[1].classList.contains("played")
        && tab[2].classList.contains("played")) {
        //shape
        //alert("C'est toi le winner au morpion !");   
        let divWin = document.createElement('div');
        divWin.classList.add('overlay');
        document.body.appendChild(divWin);
        divWin.textContent = "Tu as gagné, et c'est bien ! Voulez vous rejouer ?";
        divWin.addEventListener('click', function () {
            document.location.reload(true);
        });
    }
}


